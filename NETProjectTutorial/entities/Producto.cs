﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Producto
    {
        private int id;//4
        private string sku;//15
        private string nombre;//25
        private string descripcion;//50
        private int cantidad;//4
        private double precio; //8
        // Total => 110

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Sku
        {
            get
            {
                return sku;
            }

            set
            {
                sku = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public Producto(int id, string sku, string nombre, string descripcion, int cantidad, double precio)
        {
            this.Id = id;
            this.Sku = sku;
            this.Nombre = nombre;
            this.Descripcion = descripcion;
            this.Cantidad = cantidad;
            this.Precio = precio;
        }

       

        public override string ToString()
        {
            return Sku + " " + Nombre ;
        }

        public Producto()
        {

        }
    }
}

