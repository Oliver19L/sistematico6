﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NETProjectTutorial.model
{
    class ProductoModel
    {
        private static  List<Producto> Listproductos = new List<Producto>();
        private implements.DaoImplementsProducto daoProducto;

        public  List<Producto> GetProductos()
        {
            return daoProducto.findAll();
        }

        public ProductoModel()
        {
            daoProducto = new implements.DaoImplementsProducto();
        }

        public  void Populate()
        {
            /*Producto[] pdts =
            {
                new Producto(1,"Milk2514","Leche entera","Leche enterea 3% grasa",25,30.0),
                new Producto(2,"Milk2515","Leche descremada","Leche descremada 0% grasa",25,40.0),
                new Producto(3,"Milk2516","Leche deslactosada","Leche deslactosada 2% grasa",25,35.0)
            };*/

            Listproductos = JsonConvert.DeserializeObject<List<Producto>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Producto_data));
            foreach(Producto pro in Listproductos)
            {
                daoProducto.save(pro);
            }

          
        }
        public void save(DataRow productos)
        {

            Producto ducto = new Producto();
            ducto.Id = Convert.ToInt32(productos["Id"].ToString());
            ducto.Sku = productos["SKU"].ToString();
            ducto.Nombre = productos["Nombre"].ToString();
            ducto.Descripcion = productos["Descripcion"].ToString();
            ducto.Cantidad = Convert.ToInt32(productos["Cantidad"].ToString());
            ducto.Precio = Convert.ToDouble(productos["Precio"].ToString());
            


            daoProducto.save(ducto);



        }
    }
}
