﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.implements;
using NETProjectTutorial.dao;
using System.Data;


namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {
        private static List<Empleado> ListEmpleados = new List<Empleado>();
        private implements.DaoImplementsEmpleado daoEmpleado;
      

        public EmpleadoModel()
        {
            daoEmpleado = new implements.DaoImplementsEmpleado();
            
        }

        public List<Empleado> GetListEmpleado()
        {
            return daoEmpleado.findAll();
            
        }

        
        public void Populate()
        {
          
            ListEmpleados = JsonConvert.DeserializeObject<List<Empleado>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Empleado_data));
            foreach(Empleado  emp in ListEmpleados)
            {
               daoEmpleado.save(emp);
            }
        }

        public void save(DataRow empleados)
        {
           
                Empleado c = new Empleado();
                c.Id = Convert.ToInt32(empleados["Id"].ToString());
                c.Cedula = empleados["Cedula"].ToString();
                c.Nombre = empleados["Nombres"].ToString();
                c.Apellidos = empleados["Apellidos"].ToString();
                c.Direccion = empleados["Direccion"].ToString();
                c.Tconvencional = empleados["Telefono"].ToString();
                c.Tcelular = empleados["Celular"].ToString();
                c.Salario = Convert.ToDouble(empleados["Salario"].ToString());
                c.Sexo = (empleados["Sexo"]).ToString();
                 

                daoEmpleado.save(c);

            

        }

        public void uptade(DataRow empleado)
        {
            Empleado emple = new Empleado();

           
            empleado["Id"] = emple.Id ;
            empleado["Cedula"] = emple.Cedula;
            empleado["Nombres"] = emple.Nombre ;
            empleado["Apellidos"] = emple.Apellidos;
            empleado["Direccion"] = emple.Direccion;
            empleado["Telefono"] = emple.Tconvencional;
            empleado["Celular"] = emple.Tcelular;
            empleado["Salario"] = emple.Salario;
            empleado["Sexo"] = emple.Sexo;

        }


    }
}
