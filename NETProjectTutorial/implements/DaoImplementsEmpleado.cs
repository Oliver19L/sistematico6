﻿using NETProjectTutorial.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;
using System.IO;

namespace NETProjectTutorial.implements
{
    class DaoImplementsEmpleado : IDaoEmpleado
    {
        //header EMPLEADO
        private BinaryReader brhEmpleado;
        private BinaryWriter bwhEmpleado;
        //data Empleado
        private BinaryReader brdEmpleado;
        private BinaryWriter bwdEmpleado;

        private FileStream fshEmpleado;
        private FileStream fsdEmpleado;

        private const string FILENAME_HEADER = "hempleado.dat";
        private const string FILENAME_DATA = "dcempleado.dat";
        private const int SIZE = 400;

        private void open()
        {
            try
            {
                fsdEmpleado = new FileStream(FILENAME_DATA,
                    FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (!File.Exists(FILENAME_HEADER))
                {
                    fshEmpleado = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhEmpleado = new BinaryReader(fshEmpleado);
                    bwhEmpleado = new BinaryWriter(fshEmpleado);

                    brdEmpleado = new BinaryReader(fsdEmpleado);
                    bwdEmpleado = new BinaryWriter(fsdEmpleado);

                    bwhEmpleado.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhEmpleado.Write(0);//n
                    bwhEmpleado.Write(0);//k
                }
                else
                {
                    fshEmpleado= new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhEmpleado = new BinaryReader(fshEmpleado);
                    bwhEmpleado= new BinaryWriter(fshEmpleado);
                    brdEmpleado = new BinaryReader(fsdEmpleado);
                    bwdEmpleado = new BinaryWriter(fsdEmpleado);
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }
        private void close()
        {
            try
            {
                if (brdEmpleado != null)
                {
                    brdEmpleado.Close();
                }
                if (brhEmpleado != null)
                {
                    brhEmpleado.Close();
                }
                if (bwdEmpleado != null)
                {
                    bwdEmpleado.Close();
                }
                if (bwhEmpleado != null)
                {
                    bwhEmpleado.Close();
                }
                if (fsdEmpleado != null)
                {
                    fsdEmpleado.Close();
                }
                if (fshEmpleado != null)
                {
                    fshEmpleado.Close();
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        public List<Empleado> findAll()
        {
            open();
            List<Empleado> clientes = new List<Empleado>();

            brhEmpleado.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhEmpleado.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brhEmpleado.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brhEmpleado.ReadInt32();
                //calculamos posicion de los datos
                long dpos = (index - 1) * SIZE;
                brdEmpleado.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdEmpleado.ReadInt32();
                string Nombre = brdEmpleado.ReadString();
                string Apellido = brdEmpleado.ReadString();
                string Cedula = brdEmpleado.ReadString();
                string Inss = brdEmpleado.ReadString();
                string Direccion = brdEmpleado.ReadString();
                Double Salario = brdEmpleado.ReadDouble();
                string Tconvencional = brdEmpleado.ReadString();
                string Tcelular = brdEmpleado.ReadString();
                Sexo sexo = brdEmpleado;
               
                Empleado employ = new Empleado(id, Nombre,Apellido,Cedula, Inss, Direccion, Salario, Tconvencional, Tcelular, sexo);
                clientes.Add(employ);

               
            }

            close();
            return clientes;
           
        }

        public Empleado findByCedula(string cedula)
        {
            throw new NotImplementedException();
        }

        public Empleado findById(int id)
        {
            throw new NotImplementedException();
        }

        public List<Empleado> findByLastname(string lastname)
        {
            throw new NotImplementedException();
        }

        public void save(Empleado emple)
        {
            open();
            brhEmpleado.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhEmpleado.ReadInt32();
            int k = brhEmpleado.ReadInt32();

            long dpos = k * SIZE;
            bwdEmpleado.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdEmpleado.Write(++k);
            bwdEmpleado.Write(emple.Nombre);
            bwdEmpleado.Write(emple.Apellidos);
            bwdEmpleado.Write(emple.Cedula);
            bwdEmpleado.Write(emple.Inss);
            bwdEmpleado.Write(emple.Direccion);
            bwdEmpleado.Write(emple.Salario);
            bwdEmpleado.Write(emple.Tconvencional);
            bwdEmpleado.Write(emple.Tcelular);
            bwdEmpleado.Write((emple.Sexo).ToString());
     

            bwhEmpleado.BaseStream.Seek(0, SeekOrigin.Begin);
            bwhEmpleado.Write(++n);
            bwhEmpleado.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwhEmpleado.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwhEmpleado.Write(k);
            close();
            
        }

        public int update(Empleado t)

        {

            open();



            FileStream temp = new FileStream("Temporal.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            long hpos = 8 + 4 * (t.Id - 1);

            brhEmpleado.BaseStream.Seek(hpos, SeekOrigin.Begin);



            bwdEmpleado.Write(t.Id);
            bwdEmpleado.Write(t.Nombre);
            bwdEmpleado.Write(t.Apellidos);
            bwdEmpleado.Write(t.Cedula);
            bwdEmpleado.Write(t.Inss);
            bwdEmpleado.Write(t.Direccion);
            bwdEmpleado.Write(t.Salario);
            bwdEmpleado.Write(t.Tconvencional);
            bwdEmpleado.Write(t.Tcelular);
            bwdEmpleado.Write((t.Sexo).ToString());


           
          

            close();
            return t.Id;
           // throw new NotImplementedException();
        }

        public bool delete(Empleado t)
        {
            throw new NotImplementedException();
        }
    }
}
