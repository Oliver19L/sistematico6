﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;
using NETProjectTutorial.dao;

namespace NETProjectTutorial.implements
{
    class DaoImplementsProducto : IDaoProducto
    {
        //header EMPLEADO
        private BinaryReader brhProducto;
        private BinaryWriter bwhProducto;
        //data Empleado
        private BinaryReader brdProducto;
        private BinaryWriter bwdProducto;

        private FileStream fshProducto;
        private FileStream fsdProducto;

        private const string FILENAME_HEADER = "hemProducto.dat";
        private const string FILENAME_DATA = "dcProducto.dat";
        private const int SIZE = 390;

        private void open()
        {
            try
            {
                fsdProducto = new FileStream(FILENAME_DATA,
                    FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (!File.Exists(FILENAME_HEADER))
                {
                    fshProducto = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhProducto = new BinaryReader(fshProducto);
                    bwhProducto = new BinaryWriter(fshProducto);

                    brdProducto = new BinaryReader(fsdProducto);
                    bwdProducto = new BinaryWriter(fsdProducto);

                    bwhProducto.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhProducto.Write(0);//n
                    bwhProducto.Write(0);//k
                }
                else
                {
                    fshProducto = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhProducto = new BinaryReader(fshProducto);
                    bwhProducto = new BinaryWriter(fshProducto);
                    brdProducto = new BinaryReader(fsdProducto);
                    bwdProducto = new BinaryWriter(fsdProducto);
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        private void close()
        {
            try
            {
                if (brdProducto != null)
                {
                    brdProducto.Close();
                }
                if (brhProducto != null)
                {
                    brhProducto.Close();
                }
                if (bwdProducto != null)
                {
                    bwdProducto.Close();
                }
                if (bwhProducto != null)
                {
                    bwhProducto.Close();
                }
                if (fsdProducto != null)
                {
                    fsdProducto.Close();
                }
                if (fshProducto != null)
                {
                    fshProducto.Close();
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        public List<Producto> findAll()
        {
            open();
            List<Producto> productos = new List<Producto>();

            brhProducto.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhProducto.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brhProducto.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brhProducto.ReadInt32();
                //calculamos posicion de los datos
                long dpos = (index - 1) * SIZE;
                brdProducto.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdProducto.ReadInt32();
                string sku = brdProducto.ReadString();
                string nombre = brdProducto.ReadString();
                string descripcion = brdProducto.ReadString();
                
                int cantidad = brdProducto.ReadInt32();
                Double precio = brdProducto.ReadDouble();
               
        Producto produc = new Producto(id,  sku, nombre,descripcion, cantidad, precio);
                productos.Add(produc);


            }

            close();
            return productos;

        }
        public void save(Producto produc)
        {
            open();
            brhProducto.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhProducto.ReadInt32();
            int k = brhProducto.ReadInt32();

            long dpos = k * SIZE;
            bwdProducto.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdProducto.Write(++k);
            bwdProducto.Write(produc.Sku);
            bwdProducto.Write(produc.Nombre);
            bwdProducto.Write(produc.Descripcion);
            bwdProducto.Write(produc.Cantidad);
            bwdProducto.Write(produc.Precio);
           


            bwhProducto.BaseStream.Seek(0, SeekOrigin.Begin);
            bwhProducto.Write(++n);
            bwhProducto.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwhProducto.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwhProducto.Write(k);
            close();

        }

        public Producto findById(int id)
        {
            throw new NotImplementedException();
        }

        public List<Producto> findByLastname(string lastname)
        {
            throw new NotImplementedException();
        }

        public int update(Producto t)
        {
            open();



            FileStream temp = new FileStream("Temporal.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            long hpos = 8 + 4 * (t.Id - 1);

            brhProducto.BaseStream.Seek(hpos, SeekOrigin.Begin);



            bwdProducto.Write(t.Id);
            bwdProducto.Write(t.Sku);
            bwdProducto.Write(t.Nombre);
            bwdProducto.Write(t.Descripcion);
            bwdProducto.Write(t.Cantidad);
            bwdProducto.Write(t.Precio);
 


         

            close();
            return t.Id;
            //throw new NotImplementedException();
        }

        public bool delete(Producto t)
        {
            throw new NotImplementedException();
        }
    }
}
